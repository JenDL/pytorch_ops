#include<torch/torch.h>
#include<iostream>
#include<cmath>

void fmod_check(auto arr,float divisor)
{
    
    std::cout<<typeid(arr).name();
    torch::Tensor a = torch::div(arr, divisor);
    torch::Tensor new_a = trunc(a);
    torch::Tensor b = torch::mul(new_a, divisor);
    torch::Tensor output = torch::sub(arr, b);
    std::cout<<output;
    
}

int main()
{
    torch::Tensor arr = torch::tensor({-3, -2, -1, 0 , 1, 2, 3});
    
    torch::Tensor arr2 = torch::tensor({1,2,3,4,5});
    torch::Tensor arr3 = torch::tensor({1.8,4.2,3.7,4.5,8.5});
    fmod_check(arr,2);
    fmod_check(arr2,1.5);
    fmod_check(arr2,0);
    fmod_check(arr,0);
    fmod_check(arr3,0);
    return 0;
}