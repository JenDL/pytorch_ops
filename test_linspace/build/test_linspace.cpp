
     #include<torch/torch.h>

#include<iostream>
#include<vector>


void linspace_check(auto start,float end,auto steps)
{
    if(*typeid(steps).name()=='i')
    {
        if(steps==1)
        {
            torch::Tensor output=torch::tensor(start);
            std::cout<<output;
        }
        
        
        else if(steps==0)
        {
            torch::Tensor output=torch::empty((0));
            std::cout<<output;
        }

        else if(start==end)
        {
            auto out=torch::ones(steps);
            auto output=torch::where(out==1, start, 0);
            std::cout<<output;
            
        }
        else if(start>end)
        {
            torch::Tensor num =torch::sub(torch::tensor(end),torch::tensor(start));
            torch::Tensor den=torch::sub(torch::tensor(steps),torch::tensor(1));     
            torch::Tensor increment=torch::div(num,den);
            auto x=increment.data_ptr<float>();
            torch::Tensor output=torch::arange(start,end-1,*x);
            std::cout<<output; 

        }
        else
        {
            torch::Tensor num =torch::sub(torch::tensor(end),torch::tensor(start));
            torch::Tensor den=torch::sub(torch::tensor(steps),torch::tensor(1));     
            torch::Tensor increment=torch::div(num,den);
            auto x=increment.data_ptr<float>();
            torch::Tensor output=torch::arange(start,end+1,*x);
            std::cout<<output;    
        }
    }
    else
    {
        std::cout<<"Step value should be positive";
    }  

}

int main()
{

    linspace_check(3,10,5);
    linspace_check(-10,10,5);
    linspace_check(-10,10,1);
    linspace_check(10,8,2);
    linspace_check(5,2,0);
    linspace_check(-10,10,0);
    linspace_check(8,8,7);
    return 0;
    
} 