# README #


### What is this repository for? ###

* This repository is the C++ implementation of Pytorch OPS

### Run the following commands to build the application from respective folder

* cd test_linspace/build
* cmake -DCMAKE_PREFIX_PATH=/absolute/path/to/libtorch
* cmake --build . --config Release
* ./test_linspace


